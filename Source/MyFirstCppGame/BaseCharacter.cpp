// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ABaseCharacter::ABaseCharacter() : Super()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CountFoods = 0;
	TimerRate = 0.1f;
	DamagePerSecond = 5;

	GetMesh()->SetCollisionProfileName("SkeletalMesh");

	GetCharacterMovement()->bOrientRotationToMovement = true;

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->fDead.AddDynamic(this, &ABaseCharacter::OnDead);
}

FText ABaseCharacter::GetName()
{	
	return FText::FromString(UKismetSystemLibrary::GetDisplayName(this));
}

float ABaseCharacter::GetCountFoods()
{
	return CountFoods;
}

float ABaseCharacter::GetHealth()
{
	if(HealthComponent != nullptr)	return HealthComponent->GetHealth();
	return 0;
}

bool ABaseCharacter::IsDead()
{
	if (HealthComponent != nullptr)	return HealthComponent->isDead();
	return 0;
	
}

void ABaseCharacter::AddHealth(float AddedHealth)
{
	if (HealthComponent != nullptr)	HealthComponent->AddHealth(AddedHealth);
}

void ABaseCharacter::EatFood()
{
	CountFoods++;
	if(Sound != nullptr)
	UGameplayStatics::SpawnSoundAtLocation(this, Sound, GetActorLocation());

}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle DamageTimer;
	GetWorldTimerManager().SetTimer(DamageTimer, this, &ABaseCharacter::Damage, TimerRate, true, 0);
	
}

void ABaseCharacter::Damage()
{
	if (HealthComponent != nullptr)
	HealthComponent->Damage(TimerRate * DamagePerSecond);
	//GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, FString::FromInt(GetHealth()));
}

void ABaseCharacter::OnDead()
{
	//GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, TEXT("Dead"));
	if(AnimMontageDeath != nullptr)	PlayAnimMontage(AnimMontageDeath);
	UnPossessed();
	GetMesh()->SetSimulatePhysics(true);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

