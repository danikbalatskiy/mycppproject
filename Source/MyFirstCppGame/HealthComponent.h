// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYFIRSTCPPGAME_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintReadWrite, BlueprintAssignable, BlueprintCallable)
		FDead fDead;

	void AddHealth(float Add);
	void Damage(float Damage);

	float GetHealth();
	bool isDead();


private:
	float Health;

	UPROPERTY(EditDefaultsOnly)
	float MaxHealth;

	bool bIsDead;
		
};
