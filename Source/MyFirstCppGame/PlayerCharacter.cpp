// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"

APlayerCharacter::APlayerCharacter() : Super()
{
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 450.0f;
	CameraBoom->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FolowCamera"));
	FollowCamera->SetupAttachment(CameraBoom);
	FollowCamera->bUsePawnControlRotation = false;

	Jumping = false;
}



void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* NewPlayerInputComponent)
{
	Super::SetupPlayerInputComponent(NewPlayerInputComponent);

	NewPlayerInputComponent->BindAxis("MoveFB", this, &APlayerCharacter::MoveFB);
	NewPlayerInputComponent->BindAxis("MoveLR", this, &APlayerCharacter::MoveLR);
	NewPlayerInputComponent->BindAxis("LookLR", this, &APawn::AddControllerYawInput);
	NewPlayerInputComponent->BindAxis("LookUD", this, &APawn::AddControllerPitchInput);


	NewPlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCharacter::CheckJump);
	NewPlayerInputComponent->BindAction("Jump", IE_Released, this, &APlayerCharacter::CheckJump);
}

void APlayerCharacter::MoveFB(float Value)
{	
	Move(EAxis::X, Value);
}

void APlayerCharacter::MoveLR(float Value)
{
	Move(EAxis::Y, Value);
}

void APlayerCharacter::CheckJump()
{
	if (Jumping)
	{
		Jump();
		Jumping = false;
	}
	else
	{
		Jumping = true;
	}
}

void APlayerCharacter::Move(EAxis::Type axis, float Value)
{
	if ((Controller != NULL) && (Value != 0)) {
		FRotator Rotator = Controller->GetControlRotation();
		FRotator YawRotator(0, Rotator.Yaw, 0);
		FVector Direction = FRotationMatrix(YawRotator).GetUnitAxis(axis);
		AddMovementInput(Direction, Value);
	}
}

 
