// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyFirstCppGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYFIRSTCPPGAME_API AMyFirstCppGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AMyFirstCppGameGameModeBase();


};
