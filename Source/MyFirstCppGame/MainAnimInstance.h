// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "MainAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class MYFIRSTCPPGAME_API UMainAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

protected:
	APawn* Pawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Speed;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool IsInAir;
};
