// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseUserWidget.h"
#include "HUDWidget.generated.h"

/**
 * 
 */
UCLASS()
class MYFIRSTCPPGAME_API UHUDWidget : public UBaseUserWidget
{
	GENERATED_BODY()
	
protected:

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FText GetCountApples();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FText GetTimerText();

	UFUNCTION(BlueprintCallable)
		void UpdatePanelEnemies(UPanelWidget* Panel, TSubclassOf<AActor> FindClass, TSubclassOf<UBaseUserWidget> WidgetEnemyClass);
	

};
