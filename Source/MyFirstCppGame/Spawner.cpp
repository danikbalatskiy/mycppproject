// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ASpawner::ASpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bIsOnce = false;
	SpawnPerTime = 5;

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetCollisionProfileName(TEXT("NoCollision"));

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ASpawner::Spawn, 1, !bIsOnce, 0);
	
}

void ASpawner::Spawn()
{
	for (int i = 0; i < SpawnPerTime; i++)
	{
		FRotator Rotation(0, 0, 0);
		FVector Location = UKismetMathLibrary::RandomPointInBoundingBox(GetActorLocation(), Box->GetScaledBoxExtent());

		GetWorld()->SpawnActor<AActor>(SpawnClass, Location, Rotation);
	}
}


