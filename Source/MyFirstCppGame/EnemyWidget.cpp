// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyWidget.h"


FText UEnemyWidget::GetName()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetMainActor());
	if (Character != nullptr) return Character->GetName();
	return FText();
}

FLinearColor UEnemyWidget::GetTextColor()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetMainActor());
	if (Character->GetHealth() > 0) return ColorLive;
	return ColorDeath;
}
