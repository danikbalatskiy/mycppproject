// Fill out your copyright notice in the Description page of Project Settings.


#include "MainHUD.h"
#include "UObject/ConstructorHelpers.h"

AMainHUD::AMainHUD() : Super()
{
	ConstructorHelpers::FClassFinder<UUserWidget> WBPHUD(TEXT("/Game/UI/WBP_HUD"));
	HUDClass = WBPHUD.Class;
}

void AMainHUD::BeginPlay()
{
	Super::BeginPlay();

	if (HUDClass != nullptr)
	{
		UUserWidget* Widget = CreateWidget<UUserWidget>(GetWorld(), HUDClass);
		if (Widget)
		{
			Widget->AddToViewport();
		}
	}
}
