// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "HealthComponent.h"
#include "BaseCharacter.generated.h"


UCLASS()
class MYFIRSTCPPGAME_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

	FText GetName();

	float GetCountFoods();

	float GetHealth();

	bool IsDead();

	void AddHealth(float AddedHealth);

	void EatFood();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	int CountFoods;
	float TimerRate;

	UPROPERTY(VisibleAnywhere)
		UHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		float DamagePerSecond;

	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		USoundBase* Sound;

	UPROPERTY(EditDefaultsOnly, Category = "Settings")
		UAnimMontage* AnimMontageDeath;

	void Damage();

	UFUNCTION(BlueprintCallable)
	void OnDead();
};
