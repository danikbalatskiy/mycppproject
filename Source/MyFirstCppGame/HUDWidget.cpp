// Fill out your copyright notice in the Description page of Project Settings.


#include "HUDWidget.h"
#include "BaseCharacter.h"
#include "Kismet/KismetTextLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/PanelWidget.h"




FText UHUDWidget::GetCountApples()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetMainActor());
	if (Character != nullptr) return FloatToText(Character->GetCountFoods(), 1);
	return FText();
}

FText UHUDWidget::GetTimerText()
{
	float SecondsValue = GetWorld()->GetTimeSeconds();
	float SantisecondsValue = FMath::RoundToInt(SecondsValue * 100) % 100;
	FTimespan Timespan = FTimespan::FromSeconds(SecondsValue);

	FText Hours = FloatToText(Timespan.GetHours(),2);
	FText Minutes = FloatToText(Timespan.GetMinutes(),2);
	FText Seconds = FloatToText(Timespan.GetSeconds(),2);
	FText Santiseconds = FloatToText(SantisecondsValue, 2);

	FString StringFormat("{Hours}:{Minutes}:{Seconds}.{Santiseconds}");
	FTextFormat Format = FTextFormat::FromString(StringFormat);

	return FText::Format(Format, Hours, Minutes, Seconds, Santiseconds);
}

void UHUDWidget::UpdatePanelEnemies(UPanelWidget* Panel, TSubclassOf<AActor> FindClass, TSubclassOf<UBaseUserWidget> WidgetEnemyClass)
{
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), FindClass, OutActors);
	if (OutActors.Num() > 0)
	{
		for (AActor* Actor : OutActors)
		{
			UBaseUserWidget* Enemy = CreateWidget<UBaseUserWidget>(this, WidgetEnemyClass);
			CreateWidget<UBaseUserWidget>(this, WidgetEnemyClass);
			Panel->AddChild(Enemy);
			Enemy->SetMainActor(Actor);
		}
	}
}




