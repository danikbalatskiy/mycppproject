// Copyright Epic Games, Inc. All Rights Reserved.


#include "MyFirstCppGameGameModeBase.h"
#include "MainHUD.h"
#include "UObject/ConstructorHelpers.h"


AMyFirstCppGameGameModeBase::AMyFirstCppGameGameModeBase() : Super()
{
	ConstructorHelpers::FClassFinder<APawn> PlayerClass(TEXT("/Game/Blueprints/BP_PlayerCharacter"));
	if (PlayerClass.Class != NULL)
	{
		DefaultPawnClass = PlayerClass.Class;
	}
	HUDClass = AMainHUD::StaticClass();
}
