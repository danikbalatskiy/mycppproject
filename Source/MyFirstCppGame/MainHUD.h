// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "MainHUD.generated.h"

/**
 * 
 */
UCLASS()
class MYFIRSTCPPGAME_API AMainHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	AMainHUD();

protected:
	virtual void BeginPlay() override;

private:
	TSubclassOf<class UUserWidget> HUDClass;

};
