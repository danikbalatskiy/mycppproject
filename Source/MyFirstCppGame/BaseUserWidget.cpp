// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseUserWidget.h"
#include "BaseCharacter.h"

void UBaseUserWidget::NativeConstruct()
{
	Super::NativeConstruct();
	MainActor = GetOwningPlayerPawn();
}

AActor* UBaseUserWidget::GetMainActor()
{
	return MainActor;
}

void UBaseUserWidget::SetMainActor(AActor* Actor)
{
	MainActor = Actor;
}

FText UBaseUserWidget::GetHealth()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetMainActor());
	if (Character != nullptr) return FloatToText(Character->GetHealth(), 1, 0);
	return FText();
}

bool UBaseUserWidget::IsLiveCharacter()
{
	ABaseCharacter* Character = Cast<ABaseCharacter>(GetMainActor());
	if (Character != nullptr) return !Character->IsDead();
	return false;
}

FText UBaseUserWidget::FloatToText(float Value, int MinimumIntegralDigits, int MaximumMinimalFractionalDigits)
{
	return UKismetTextLibrary::Conv_FloatToText(Value, ERoundingMode::FromZero, false, false, MinimumIntegralDigits, 324, 0, MaximumMinimalFractionalDigits);
}
