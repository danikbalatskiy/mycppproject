// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseUserWidget.h"
#include"BaseCharacter.h"
#include "EnemyWidget.generated.h"

/**
 * 
 */
UCLASS()
class MYFIRSTCPPGAME_API UEnemyWidget : public UBaseUserWidget
{
	GENERATED_BODY()


protected:
	UFUNCTION(BlueprintCallable, BlueprintPure)
		FText GetName();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FLinearColor GetTextColor();

private:
	UPROPERTY(EditDefaultsOnly)
		FLinearColor ColorLive;
	UPROPERTY(EditDefaultsOnly)
		FLinearColor ColorDeath;
};
