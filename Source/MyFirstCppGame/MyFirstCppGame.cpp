// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyFirstCppGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MyFirstCppGame, "MyFirstCppGame" );
