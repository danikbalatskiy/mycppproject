// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/KismetTextLibrary.h"
#include "BaseUserWidget.generated.h"


/**
 * 
 */
UCLASS()
class MYFIRSTCPPGAME_API UBaseUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetMainActor(AActor* Actor);
protected:

	virtual void NativeConstruct() override;
	AActor* GetMainActor();
	

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FText GetHealth();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsLiveCharacter();


	FText FloatToText(float Value, int MinimumIntegralDigits = 2, int MaximumMinimalFractionalDigits = 2);

private:
	AActor* MainActor;

};
