// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	bIsDead = false;
	MaxHealth = 100;
	Health = MaxHealth;
}

void UHealthComponent::AddHealth(float Add)
{
	float Temp = Health + Add;
	Health = FMath::Min(Temp, MaxHealth);
}

void UHealthComponent::Damage(float Damage)
{
	float Temp = Health - Damage;
	Health = FMath::Max(Temp, 0.0f);
	if (Health == 0 && !bIsDead)
	{
		bIsDead = true;
		fDead.Broadcast();
	}
}

float UHealthComponent::GetHealth()
{
	return Health;
}

bool UHealthComponent::isDead()
{
	return bIsDead;
}




