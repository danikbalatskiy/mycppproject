// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Character.h"
#include "Components/InputComponent.h"

#include "PlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class MYFIRSTCPPGAME_API APlayerCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	APlayerCharacter();

	virtual void SetupPlayerInputComponent(class UInputComponent* NewPlayerInputComponent) override;
	
	
private:
	UPROPERTY(VisibleAnywhere)
	USpringArmComponent* CameraBoom; 

	UPROPERTY(VisibleAnywhere)
	UCameraComponent* FollowCamera;

	void MoveFB(float Value);
	void MoveLR(float Value);
	void CheckJump();
	
	UPROPERTY()
		bool Jumping;

	void Move(EAxis::Type axis, float Value);
};
